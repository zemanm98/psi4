# PSI4 - Proveďte nastavení malé domácí sítě připojené do internetu přes dva směrovače a NAT podle uvedeného schématu a simulátoru GNS3. Použijte uvedený adresní prostor, který rozdělíte na dvě podsítě podle doporučení ve schématu


## Pohled na systém
<img src="img/system.png">

## Router R1

Nastavení rozhraní routeru směřujícímu k R2
```
config term
interface gigabitEthernet 1/0
ip address 192.168.1.1 255.255.255.252
no shutdown
end
```

Nastavení pro protokol RIP a jeho zprávy
```
config term
router rip
version 2
network 192.168.1.0
end
```

Nastavení rozhraní routeru k poskytovateli internetového připojení
```
config term
interface gigabitEthernet 0/0
ip address dhcp
no shutdown
end
```

Nastavení NAT
```
config term
access-list 100 permit ip 192.168.1.0 0.0.0.3 any
access-list 100 permit ip 10.10.2.0 0.0.0.31 any
interface gigabitEthernet 1/0
ip nat inside
interface gigabitEthernet 0/0
ip nat outside
ip nat inside source list 100 interface GigabitEthernet0/0 overload
end
```

### Router R2

Nejprve je nutné přiřadit IP adresu k rozhraní gigabitEthernet 1/0 routeru
```
config term
interface gigabitEthernet 1/0
ip address 10.10.2.2 255.255.255.224
no shutdown
end
```

Poté je nutné nastavit DHCP pro přiřazení adres hostům a zařízením + stanovení IP DNS serveru
```
config term
ip dhcp pool home
network 10.10.2.0 255.255.255.224
default-router 10.10.2.2
dns-server 8.8.8.8 4.4.4.4
end
```

Nastavení rozhraní směřujícímu k R1
```
config term
interface gigabitEthernet 0/0
ip address 192.168.1.2 255.255.255.252
no shutdown
end
```

Nastavení RIP
```
config term
router rip
version 2
network 10.10.2.0
network 192.168.1.0
end
```

### Výsledek
S těmito příkazy je možné z počítačových nodes pingovat do vnější sítě. Např. na doménu seznam.cz nebo google.com
